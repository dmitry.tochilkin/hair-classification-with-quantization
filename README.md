#### A simle lightweight (<1.5 MB) models for classifying type of hair (long/short). ####

## Launch ##
To launch script run 
```
python classify_hair.py hair-val/val
```
or you can launch quantized model for running on CPU:
```
python classify_hair.py hair-val/val -q
```
Code was tested using PyTorch 1.8.0 with CUDA 11.
Prerequisites are described in `requirements.txt`

## Project description ##
#### Classification model ####
As a base model pretrained `mobilenet_v3_small` from PyTorch model zoo was selected. 
There are two models implemented: a truncated vesion of the model that runs on GPU and a quantized version that runs on CPU.

In order to shrink model sizes classification head number of *in_features* was decreased. Final model weight: 1.40 MB

For the truncated version several final feature layers of the base network were also replaced with *nn.Identity* modules. For the quantized model, [quantized transfer learning](https://pytorch.org/tutorials/intermediate/quantized_transfer_learning_tutorial.html) was used. Several `mobilenet_v3` submodules were rewritten in order to support quantization. Please note that only feature extraction modules are quantized, classifier head remains float which means that you can speed up inference by pushing tensors to GPU for classification (will be added later). Final model weight: 1.45 MB

#### Face detection module ####
As a face detection module MTCNN model from [facenet repository](https://github.com/timesler/facenet-pytorch#guide-to-mtcnn-in-facenet-pytorch) was used. Also, several helper functions from the repository were used during training.

#### Dataset ####
As training and validation datasets `hair-train` subset combined with [`hairstyle30k`](https://yanweifu.github.io/papers/hairstyle_v_14_weidong.pdf) dataset were used.

#### Metrics ####
| Model       | Acc                |
| ------------- |:------------------:|
| **truncated**     | **0.9904**    |
| **quantized**     | **0.9612** |

## Next steps to improve on ##
##### Model accuracy #####
Investigate false predictions, a part of them will correspond to inaccurate labels -> improve labels. Another part will correspond to wrong model performance -> experiment with other training schedules, loss functions and architectures. Augment severely false prediction images and add to the dataset. Experiment with face crop margins 

##### Quantization #####
Experiment with other types of observers

