import argparse
import os
from PIL import Image
import torch
from torch.utils.data import DataLoader
import torch.nn as nn
from torchvision import datasets, models, transforms
from facenet_pytorch import MTCNN, training
import warnings

from mobilenet_v3_quant_supported import MobileNetV3, mobilenet_v3_small
import utils

parser = argparse.ArgumentParser()
parser.add_argument("datadir", help="path to a directory containing .jpg, .jpeg and .png images to classify")
parser.add_argument("-q", "--quantized", action="store_true", help="whether to use quantized model")
args = parser.parse_args()

batch_size = 1
workers = 0 if os.name == 'nt' else 2
detection_threshold = 0.6
device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')

if not args.quantized:
    model = torch.load('models/truncated/mobilenet_v3_small_19.pth')
    model.to(device)
else:
    # Initialize mobilenet_v3 classification model
    model = mobilenet_v3_small()
    model.classifier[0] = nn.Linear(576, 128)
    model.classifier[3] = nn.Linear(128, 1)
    model.eval();

    # Configure quantization
    # Significantly more stable training
    qact = torch.quantization.MovingAverageMinMaxObserver.with_args(reduce_range=True)
    qconfig = torch.quantization.QConfig(
        activation=qact, weight=torch.quantization.observer.default_weight_observer)    
    backend = "fbgemm"  # switch to "qnnpack" for mobile inference
    model = utils.wrap_with_quant_stubs(model)
    model.qconfig = qconfig  # torch.quantization.get_default_qat_qconfig(backend)
    model[-1].qconfig = None  # Turn off quantization for classification head
    torch.backends.quantized.engine = backend
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        torch.quantization.prepare_qat(model, inplace=True)

    # Load trained prepared model
    model.load_state_dict(torch.load('models/quantized2/mobilenet_v3_small_10_prepared.pth'))
    model.to('cpu')
    
    # Quantize model
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        model = torch.quantization.convert(model, inplace=False)
    utils.set_hardsigmoid_quantized(model, True)
    
model.eval()

# Initialize face detector
# print('Running on device: {}'.format(device))
mtcnn = MTCNN(
    image_size=224, margin=70, min_face_size=70,
    thresholds=[0.6, 0.7, 0.7], factor=0.709,
    post_process=True, select_largest=False,
    device=device,
)

resize = transforms.Resize(512)
norm = transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
with open('result.csv','w') as file:
    for path in os.listdir(args.datadir):
        if not path.endswith(('.jpg', '.jpeg', 'png')):
            continue
        path = os.path.join(args.datadir, path)
        sample = [resize(Image.open(path))]
        crops = mtcnn(sample, return_prob=True)
        if crops[1].item() is None or crops[1][0] < detection_threshold:
            file.write("{},{}\n".format(os.path.abspath(path), -1))
            continue
        crop = crops[0][0]
        logit = model(norm(crop.unsqueeze(0)).to(next(model.parameters()).device))
        label = int(torch.round(torch.sigmoid(logit)).item())
        file.write("{},{}\n".format(os.path.abspath(path), label))