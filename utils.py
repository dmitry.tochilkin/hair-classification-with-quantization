import os
import torch
import torch.nn as nn
from torchvision.transforms.functional import crop
import mobilenet_v3_quant_supported
from facenet_pytorch import training


def get_model_size(mdl):
    torch.save(mdl.state_dict(), "tmp.pt")
    size_str = "%.2f MB" %(os.path.getsize("tmp.pt")/1e6)
    os.remove('tmp.pt')
    return size_str
    
def top_center_crop(img):
    if img.height > img.width:
        return crop(img, 0, 0, img.width, img.width)
    else:
        return crop(img, 0, (img.width - img.height) // 2, img.height, img.height)

def wrap_with_quant_stubs(model):
    quant = torch.quantization.QuantStub()
    dequant = torch.quantization.DeQuantStub()
    
    return nn.Sequential(
        quant,
        model.features,
        nn.AdaptiveAvgPool2d(output_size=(1, 1)),
        dequant,
        nn.Flatten(1),
        model.classifier,
    )

def set_hardsigmoid_quantized(module, value):
    gen = module.children()
    for m in gen:
        set_hardsigmoid_quantized(m, value)
        if type(m) == mobilenet_v3_quant_supported.SqueezeExcitation:
            m.hardsigmoid_quantized = value
            
def pass_epoch(model, loss_fn, loader, optimizer=None, scheduler=None,
               batch_metrics={'time': training.BatchTimer()}, show_running=True,
               device='cpu', writer=None):
    mode = 'Train' if model.training else 'Valid'
    logger = training.Logger(mode, length=len(loader), calculate_mean=show_running)
    loss = 0
    metrics = {}

    for i_batch, (x, y) in enumerate(loader):
        x = x.to(device)
        y = y.type(torch.FloatTensor).to(device)
        y_pred = model(x).squeeze()
#         print(y.size(), y_pred.size())
        loss_batch = loss_fn(y_pred, y)

        if model.training:
            loss_batch.backward()
            optimizer.step()
            optimizer.zero_grad()

        metrics_batch = {}
        for metric_name, metric_fn in batch_metrics.items():
            metrics_batch[metric_name] = metric_fn(y_pred, y).detach().cpu()
            metrics[metric_name] = metrics.get(metric_name, 0) + metrics_batch[metric_name]
            
        if writer is not None and model.training:
            if writer.iteration % writer.interval == 0:
                writer.add_scalars('loss', {mode: loss_batch.detach().cpu()}, writer.iteration)
                for metric_name, metric_batch in metrics_batch.items():
                    writer.add_scalars(metric_name, {mode: metric_batch}, writer.iteration)
            writer.iteration += 1
        
        loss_batch = loss_batch.detach().cpu()
        loss += loss_batch
        if show_running:
            logger(loss, metrics, i_batch)
        else:
            logger(loss_batch, metrics_batch, i_batch)
    
    if model.training and scheduler is not None:
        scheduler.step()

    loss = loss / (i_batch + 1)
    metrics = {k: v / (i_batch + 1) for k, v in metrics.items()}
            
    if writer is not None and not model.training:
        writer.add_scalars('loss', {mode: loss.detach()}, writer.iteration)
        for metric_name, metric in metrics.items():
            writer.add_scalars(metric_name, {mode: metric})

    return loss, metrics